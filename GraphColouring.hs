{-
This module has a Graph data type, can generate tori and hypercubes
and compute a proper vertex colouring of a generated Graph.

author: MrSidewinder
-}

module GraphColouring where
import Data.Char
import Data.List
import Data.Maybe

-- The standard vertex has simply a number (name) and a 
-- list of adjacent vertices.
type Vertex = (Int,[Int])

-- Represents an edge in a graph. From the name of one vertex to another.
type Edge = (Int,Int)

-- The coloured vertex has one further element, denoting it's colour.
-- The form used is (number,colour,adjacency list).
-- A ColouredVertex with colour -1 is uncoloured.
type ColouredVertex = (Int,Int,[Int])

-- A graph is a list of Vertex types.
data Graph = Graph {vertices :: [Vertex]} deriving (Show)

-- Colours a graph and returns the colours given to each vertex in order and the
-- number of colours used.
greedycolouring :: Graph -> ([Int],Int)
greedycolouring g = (cList,length (nub cList)) 
                where
                vList = setUncoloured (vertices g) -- Vertex List, initially uncoloured.
                cList = getColourList (colourGraph vList) -- List of colours

-- Retrieves the colours used in a list of coloured vertices.
getColourList :: [ColouredVertex] -> [Int]
getColourList cv = [c | (n,c,a) <- cv]

-- Converts a list of standard vertices into coloured vertices 
-- with no colour (colour = -1).
setUncoloured :: [Vertex] -> [ColouredVertex]
setUncoloured vList = [(v,-1,a) | (v,a) <- vList]

--Colours a list of vertices.
colourGraph :: [ColouredVertex] -> [ColouredVertex]
colourGraph [] = []
-- The list is traversed recursively by appending the coloured vertex to the
-- end of the list of vertices.
colourGraph vList = if c /= -1 then [] else colV:colourGraph ((tail vList) ++ [colV]) 
                 where 
                 (v,c,a) = head vList
                 colV = colourVertex (v,c,a) vList

-- Colours a single vertex using a list of coloured vertices.
colourVertex :: ColouredVertex -> [ColouredVertex] -> ColouredVertex
-- The first vertex in a list is coloured 0.
colourVertex (num,colour,aList) [] = (num,0,aList)
colourVertex (num,colour,aList) colouredSection = (num,getColour (getUsedColours aList colouredSection), aList)

-- Retrieves the colours used from a set of coloured vertices.
getUsedColours :: [Int] -> [ColouredVertex] -> [Int]
getUsedColours aList vList = [c | adjVertex <- aList, (n,c,a) <- vList, adjVertex == n, c /= -1]

-- Takes a list of used colours and returns the smallest colour not used.
getColour :: [Int] -> Int
getColour [] = 0 -- If no colours have been used, the colour is 0.
-- Add only those colours not used (in order).
getColour used = if ((length unused) == 0) then (length used) else unused !! 0 
                 where 
                 unused = [c | c <- [0..(length used) - 1],notElem c used]
 
-- Generates an x by y torus as a graph.
genTorus :: Int -> Int -> Graph
genTorus x y = Graph ([(num,(getAdjacentList num edges)) | num <- [0..(x*y-1)]])
                  where
                  -- The vertices of the torus.
                  vert = getTorusVertices x y 
                  -- The edges, with duplicates removed.
                  edges = nameEdges (nubSym (getTorusEdges vert x y)) vert

-- Generates the vertices of an x by y torus. Named as coordinates.
getTorusVertices :: Int -> Int -> [(Int,Int)]
getTorusVertices x y = [(i,j) | i <- [0..x-1], j <- [0..y-1]]

-- Generates the edges of an x by y torus in the form ((i,j),(k,l)) given a
-- list of torus vertices, where (i,j) and (k,l) are torus vertices.
getTorusEdges :: [(Int,Int)] -> Int -> Int -> [((Int,Int),(Int,Int))]
getTorusEdges vList x y = [((i,j),(k,l)) | (i,j) <- vList, (k,l) <- vList, hasEdge k i x l j || hasEdge l j y k i, not ((i,j) == (k,l))]

-- Given the numbers of two Torus vertices determines whether an 
-- edge exists between them.
hasEdge :: Int -> Int -> Int -> Int -> Int -> Bool
hasEdge a b x c d = if a == ((b+1) `mod` x) then (c == d) else if a == ((b-1) `mod` x) then (c == d) else False

-- Gets a list of adjacent vertices of a vertex given a set of edges.
getAdjacentList :: Int -> [Edge] -> [Int]
getAdjacentList v edgeList = [if (v == b) then a else b | (a,b) <- edgeList, (v == a|| v == b)]

-- Given a list of edges and vertices returns a list of edges in a simple form.
nameEdges :: Eq a => [(a,a)] -> [a] -> [(Int, Int)]
nameEdges eList vList = map (translateEdge vList) eList

-- Translates an edge into a standard form (Int,Int) given a list of vertices.
translateEdge :: Eq a => [a] -> (a,a) -> Edge
translateEdge vList e = (fromJust (elemIndex (fst e) vList), fromJust (elemIndex (snd e) vList))

-- Generates an n-dimensinal hypercube as a graph.
genHypercube :: Int -> Graph
genHypercube n = Graph ([(num,(getAdjacentList num edges)) | num <- [0..(2^n)-1]])
                     where
                     vert = genbin n
                     edges = nameEdges (nubSym (getConnected (vert))) vert

-- Generates all binary strings of length n.
genbin :: Int -> [String]
genbin 0 = []
genbin 1 = ["0", "1"]
genbin i =
    map ('0' :) x ++ map ('1' :) x
                     where
                     x = genbin (i - 1)

-- Removes duplicate tuples from a list where (a,b) == (b,a).
-- i.e symmetrical tuples are considered equal. 
nubSym :: Ord a => [(a,a)] -> [(a,a)]
nubSym xs = nub (map fix xs)
                     where fix (a,b) | a > b     = (b,a)
                                     | otherwise = (a,b)

-- Given a list of bit strings returns a list of those that are connected in
-- a hypercube.
getConnected :: [String] -> [(String,String)]
getConnected xs = [(a,b) | a <- xs, b <- xs, areConnected a b]

-- Determines whether two bit strings are connected in a hypercube.
-- They are connected iff the number of differences in their characters is 1.
areConnected :: String -> String -> Bool
areConnected a b  = (numDifferent (zip a b)) == 1

-- Given a list of tuples returns the number where both elements are not equal. 
numDifferent :: [(Char,Char)]  -> Int
numDifferent pairs = length diff
                       where diff = [(a,b) | (a,b) <- pairs, not (a == b)]

-- The two following methods are leftovers from tests to determine whether
-- the order in which vertices are coloured have any effect on the number of
-- colours needed in a proper vertex colouring.

-- Sorts coloured vertices by name.
sortByName (n1,c1,a1) (n2,c2,a2)
  | n1 <= n2 = LT
  | n1 > n2 = GT

-- Sorts standard vertices by their degree (size of adjacency list).
sortByDegree (n1,a1) (n2,a2)
  | length a1 <= length a2 = LT
  | length a1 > length a2 = GT